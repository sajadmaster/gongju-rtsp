var express = require("express");
var app = express();
var cctvStreamStart = require("./api/cctvStreamStart");
var cctvStreamStop = require("./api/cctvStreamStop");
var StreamModel = require("./model/StreamModel");

var myStreamModel = new StreamModel();

// start server
app.listen(3001, () => {
  console.log("Server Listening on: port 3001");

});

// /cctvStreamStart API
app.get("/cctvStreamStart", (req, res, next) => {
  myStreamModel = cctvStreamStart(req, res);

  
});

// cctvStreamStop
app.get("/cctvStreamStop", (req, res, next) => {
  cctvStreamStop(req, res, myStreamModel);
});