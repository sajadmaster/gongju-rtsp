const AppSettings = require("../AppSettings");
Stream = require("node-rtsp-stream");
var StreamModel = require("../model/StreamModel");

var myStreamModel = new StreamModel();

module.exports = function StartStreaming(streamModel) {
  var stream = new Stream({
    name: "name",
    streamUrl: streamModel.getRtspUrl(),
    wsPort: streamModel.getStreamPort(),

    ffmpegOptions: {
      // options ffmpeg flags
      // "-stats": "",
      "-r": 30,
      "-y": "",
      "-f": "mpegts",
      "-codec:v": "mpeg1video",
      "-b:v": "700k",
      "-r": "25",
      "-bf": "0",
      "-codec:a": "mp2",
      "-ar": "44100",
      "-ac": "1",
      "-b:a": "64k",
    },
  });

  myStreamModel = streamModel;
  myStreamModel.setStream(stream);

  console.log("sajad myStreamModel12 : " + myStreamModel.getRtspUrl());
  return myStreamModel;
  
  // stop stream after 10 secs
  //   console.log("before sleep");
  //   await sleep(10000);
  //   stream.stop();
  //   console.log("after sleep");

  //   function sleep(ms) {
  //     return new Promise((resolve) => {
  //       setTimeout(resolve, ms);
  //     });
  //   }
}

