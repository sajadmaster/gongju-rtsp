var StreamModel = require("../model/StreamModel");
var myStreamModel = new StreamModel();

module.exports = function cctvStreamStop(req, res, streamModel) {
    console.log("Inside cctvStreamStop API");
    myStreamModel = streamModel;

    if (myStreamModel.getStream() != null){
        myStreamModel.getStream().stop();
        console.log("Closed a port");
    }{
        console.log("Did not close a port because nothing is streaming.");
    }


    // Send Response back to front
    sendResponseToFront(res);
    // res.setHeader('Access-Control-Allow-Origin', '*');
    // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    // res.setHeader('Access-Control-Allow-Credentials', true);
    // res.send({
    //     stat: "Streaming stopped.",
    //     code: "OK"
    // });
}