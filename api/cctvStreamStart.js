const AppSettings = require("../AppSettings");
var StartStreaming = require("../Controller/StartStreaming");
var GetFixedResHeaders = require("../Controller/GetFixedResHeaders");
var tcpPortUsed = require("tcp-port-used");
var StreamModel = require("../model/StreamModel");


// Initializing variables
var port = AppSettings.INIT_PORT;
var myStreamModel = new StreamModel();

module.exports = function cctvStreamStart(req, res) {
    // /cctvStream API
    myStreamModel.setRtspUrl(req.query.rtspurl);
    console.log("requested RTSP URL: " + myStreamModel.getRtspUrl);
    
    // check if port is busy serving another stream
    tcpPortUsed.check(port, "localhost").then(
        function (inUse) {
            console.log("Port usage: " + inUse);
            myStreamModel.setStreamPort(port);
            if (inUse) {
                // rtsp is already streaming, send the streaming port to user
                console.log("port is inUse true. port: " + port + ". The port will be increased by 1.");
                port = port + 1;
                myStreamModel.setStreamPort(port); // update streaming port in our model
                myStreamModel = StartStreaming(myStreamModel);
            } else {
                console.log("port is NOT inUse. port: " + port);
                myStreamModel = StartStreaming(myStreamModel);
            }
        },
        function (err) {
            console.error("Error on check:", err.message);
        }
    );

    // Send Response back to front
    res = GetFixedResHeaders(res);
    res.send({
        stat: "Streaming Started.",
        url: AppSettings.WS_URL,
        port: port,
    });

    return myStreamModel;

}