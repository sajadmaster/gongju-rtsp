module.exports = Object.freeze({
    WS_URL: "ws://localhost",
    INIT_PORT: 3100, // The initial port value used for streaming. This value is increased by 1 for every new connection request.
});