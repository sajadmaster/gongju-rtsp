function StreamModel(rtspUrl, streamPort, stream) { 
    this.rtspUrl = rtspUrl || null;
    this.streamPort  = streamPort  || null;
    this.stream = stream || null;
}

StreamModel.prototype.getRtspUrl = function() {
    return this.rtspUrl;
}

StreamModel.prototype.setRtspUrl = function(rtspUrl) {
    this.rtspUrl = rtspUrl;
}

StreamModel.prototype.getStreamPort = function() {
    return this.streamPort;
}

StreamModel.prototype.setStreamPort = function(streamPort) {
    this.streamPort = streamPort;
}

StreamModel.prototype.getStream = function() {
    return this.stream;
}

StreamModel.prototype.setStream = function(stream) {
    this.stream = stream;
}

module.exports = StreamModel;     // Export the Cat function as it is